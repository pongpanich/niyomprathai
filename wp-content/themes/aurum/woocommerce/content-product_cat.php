<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.2
 */

/* Note: This file has been altered by Laborator */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Increase loop count
$woocommerce_loop['loop'] ++;

# start: modified by Arlind Nushi
$category_columns = get_data('shop_category_columns');
$item_colums = '';

switch($category_columns)
{
	case 4:
		$item_colums = 'col-lg-3 col-md-3 col-sm-6';
		break;

	case 3:
		$item_colums = 'col-lg-4 col-md-4 col-sm-6';
		break;

	case 2:
		$item_colums = 'col-xs-6';
		break;
}

$do_clear = $woocommerce_loop['loop'] % $category_columns == 0;
# end: modified by Arlind Nushi
?>
<div <?php wc_product_cat_class( $item_colums, $category ); ?>>
	<div class="product-category product<?php
	    if ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 || $woocommerce_loop['columns'] == 1 )
	        echo ' first';
		if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
			echo ' last';

		?>">

		<?php do_action( 'woocommerce_before_subcategory', $category ); ?>

		<a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>">

			<?php
				/**
				 * woocommerce_before_subcategory_title hook
				 *
				 * @hooked woocommerce_subcategory_thumbnail - 10
				 */
				do_action( 'woocommerce_before_subcategory_title', $category );

				# start: modified by Arlind Nushi
				$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true  );
				$thumbnail_url = wc_placeholder_img_src();

				if($thumbnail_id)
				{
					$thumbnail_url_custom = wp_get_attachment_image_src( $thumbnail_id, apply_filters('laborator_wc_category_thumbnail_size', 'shop-category-thumb') );

					if($thumbnail_url_custom)
						$thumbnail_url = $thumbnail_url_custom[0];
				}

				echo '<img src="'.$thumbnail_url.'" alt="category-shop" />';
				# end: modified by Arlind Nushi
			?>

			<span>
				<?php
					echo $category->name;

					if ( $category->count > 0 && get_data('shop_category_count') )
						echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">(' . $category->count . ')</mark>', $category );
				?>
			</span>

			<?php
				/**
				 * woocommerce_after_subcategory_title hook
				 */
				do_action( 'woocommerce_after_subcategory_title', $category );
			?>

		</a>

		<?php do_action( 'woocommerce_after_subcategory', $category ); ?>

	</div>
</div>


<?php
if($do_clear):

	switch($category_columns):

		case 3: 
			?>
			<div class="clear-md"></div>
			<?php 
			break;
			
		default: 
			?>
			<div class="clear"></div>
			<?php

	endswitch;

endif;
?>