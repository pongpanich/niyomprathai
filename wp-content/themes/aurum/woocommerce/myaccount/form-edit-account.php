<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.1
 */

/* Note: This file has been altered by Laborator */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

# start: modified by Arlind Nushi
include locate_template( 'tpls/woocommerce-account-tabs-before.php' );
# end: modified by Arlind Nushi
?>

<div class="page-title">
	<h2>
		<?php _e('Edit Account <small>Change your name, email or password</small>', 'aurum'); ?>
	</h2>
</div>

<?php wc_print_notices(); ?>

<form class="edit-account" action="" method="post">

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
	
	<fieldset>
		<legend class="no-top-margin"><?php _e( 'Name and Email', 'aurum' ); ?></legend>
	
		<p class="form-row form-row-first">
			<input type="text" class="input-text form-control" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" placeholder="<?php _e( 'First name', 'aurum' ); ?> *" />
		</p>
		<p class="form-row form-row-last">
			<input type="text" class="input-text form-control" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" placeholder="<?php _e( 'Last name', 'aurum' ); ?> *" />
		</p>
		<div class="clear"></div>
	
		<p class="form-row form-row-wide">
			<input type="email" class="input-text form-control" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" placeholder="<?php _e( 'Email address', 'aurum' ); ?> *" />
		</p>
	</fieldset>

	<fieldset>
		<legend><?php _e( 'Password Change', 'aurum' ); ?></legend>

		<p class="form-row form-row-wide">
			<input type="password" class="input-text form-control" name="password_current" id="password_current" placeholder="<?php _e( 'Current Password (leave blank to leave unchanged)', 'aurum' ); ?>" />
		</p>
		<p class="form-row form-row-wide">
			<input type="password" class="input-text form-control" name="password_1" id="password_1" placeholder="<?php _e( 'New Password (leave blank to leave unchanged)', 'aurum' ); ?>" />
		</p>
		<p class="form-row form-row-wide">
			<input type="password" class="input-text form-control" name="password_2" id="password_2" placeholder="<?php _e( 'Confirm New Password', 'aurum' ); ?>" />
		</p>
	</fieldset>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details' ); ?>
		<input type="submit" class="button btn btn-primary" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'aurum' ); ?>" />
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>

</form>

<?php

# start: modified by Arlind Nushi
include locate_template( 'tpls/woocommerce-account-tabs-after.php' );
# end: modified by Arlind Nushi